﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
 rueckgeldAusgeben(fahrkartenBezahlen(fahrkartenbestellungErfassen())); 

    }


     public static double fahrkartenbestellungErfassen() {
         byte anzahlFahrkarten;
         double zuZahlenderBetrag;
         byte ArtFahrkarte;
         double Einzelpreis = 0;
         Scanner tastatur = new Scanner(System.in);
         //System.out.print("Zu zahlender Betrag (EURO): ");
         do {
         System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n" + 
         		"  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + 
         		"  Tageskarte Regeltarif AB [8,60 EUR] (2)\n" + 
         		"  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
         ArtFahrkarte = tastatur.nextByte();
	         if (ArtFahrkarte == 1) {
	        	 
	        	Einzelpreis = 2.90;
	         }
	         else if (ArtFahrkarte == 2) {
	        	 
	         	Einzelpreis = 8.60;
	          }
	         else if (ArtFahrkarte == 3) {
	        	 
	         	Einzelpreis = 23.50;
	          }
	         else {
	        	 System.out.print(" >>falsche Eingabe<<\n\n");
	         }
	         
         }
         while ((ArtFahrkarte != 1)&&(ArtFahrkarte != 2)&&(ArtFahrkarte != 3));
         
         System.out.print("Anzahl der Fahrkarten: \n");
         anzahlFahrkarten = tastatur.nextByte();
         if (anzahlFahrkarten > 10) {
        	  System.out.print("\nIhre gewünschte Fahrkartenanzahl kann NICHT bearbeitet werden! \nIhre Anzahl wurde auf EINE Fahrkarte angepasst.\n");
        	  anzahlFahrkarten = 1;
        	  zuZahlenderBetrag = (Einzelpreis * anzahlFahrkarten);
        	  return zuZahlenderBetrag;
         }
         else {
       	  zuZahlenderBetrag = (Einzelpreis * anzahlFahrkarten);
        	  return zuZahlenderBetrag;
         }


         
     }
     public static double fahrkartenBezahlen(double zuZahlen) {
         double eingezahlterGesamtbetrag;
         double eingeworfeneMünze;
         double rückgabebetrag1;
         Scanner tastatur = new Scanner(System.in);
    	 eingezahlterGesamtbetrag = 0.0;
    	 
    	 
         while(eingezahlterGesamtbetrag < zuZahlen/*/derBetrag/*/)
         {
      	   System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", ((zuZahlen/*/derBetrag/*/)-(eingezahlterGesamtbetrag)), " Euro");
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMünze;
             
         }
         rückgabebetrag1 = eingezahlterGesamtbetrag - zuZahlen/*/derBetrag/*/;
         return rückgabebetrag1;
     }
    

    	public static void fahrkartenAusgeben() {

    		       System.out.println("\nFahrscheine werden ausgegeben");
    		       for (int i = 0; i < 8; i++)
    		       {
    		          System.out.print("=");
    		          try {
    					Thread.sleep(250);
    				} catch (InterruptedException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    		       }
    		       System.out.println("\n\n");

    	}
    	

    	public static void rueckgeldAusgeben(double rückgabebetrag) {
    		fahrkartenAusgeben();
    		 if(rückgabebetrag > 0.0)
    	       {
    	    	   System.out.printf("%s%.2f%s","Der Rückgabebetrag in Höhe von ",(rückgabebetrag)," Euro ");
    	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

    	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
    	           {
    	        	  System.out.println("2,00 Euro");
    		          rückgabebetrag -= 2.0;
    	           }
    	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
    	           {
    	        	  System.out.println("1,00 Euro");
    		          rückgabebetrag -= 1.0;
    	           }
    	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
    	           {
    	        	  System.out.println("50 CENT");
    		          rückgabebetrag -= 0.5;
    	           }
    	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
    	           {
    	        	  System.out.println("20 CENT");
    	 	          rückgabebetrag -= 0.2;
    	           }
    	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
    	           {
    	        	  System.out.println("10 CENT");
    		          rückgabebetrag -= 0.1;
    	           }
    	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
    	           {
    	        	  System.out.println("5 CENT");
    	 	          rückgabebetrag -= 0.05;
    	           }
        	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n"+
	                          "\n Der Vorgang wird nun neugestartet.\n" );
    		       for (int i = 0; i < 8; i++)
    		       {
    		          System.out.print("=");
    		          try {
    					Thread.sleep(250);
    				} catch (InterruptedException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    		       }
    		       System.out.println("\n\n");
    		       rueckgeldAusgeben(fahrkartenBezahlen(fahrkartenbestellungErfassen()));
    	       }
    		 else {
      	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.\n"+
      	    		   "\n Der Vorgang wird nun neugestartet.\n");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n");
  		       rueckgeldAusgeben(fahrkartenBezahlen(fahrkartenbestellungErfassen()));
    		 }


    	}

}