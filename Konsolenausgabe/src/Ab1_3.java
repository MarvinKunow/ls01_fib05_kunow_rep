
public class Ab1_3 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			String a = "Fahrenheit";
			String b = "Celsius";
			double  f1 = -20;
			double f2 = -10;
			double f3 = 0;
			double f4 = 20;
			double f5 = 30;
			
			System.out.printf("%-12s|%10s\n", a, b);
			System.out.printf("------------------------\n");
			System.out.printf("%-12s", f1);
			System.out.printf("%-5s","|");
			System.out.printf("%.2f\n",(f1-32)/1.8);
			System.out.printf("%-12s", f2);
			System.out.printf("%-5s","|");
			System.out.printf("%.2f\n",(f2-32)/1.8);
			System.out.printf("%-12s", f3);
			System.out.printf("%-5s","|");
			System.out.printf("%.2f\n",(f3-32)/1.8);
			System.out.printf("%-12s", f4);
			System.out.printf("%-5s","|");
			System.out.printf("%.2f\n",(f4-32)/1.8);
			System.out.printf("%-12s", f5);
			System.out.printf("%-5s","|");
			System.out.printf("%.2f\n",(f5-32)/1.8);
			
			//Da meine Loesung etwas komplizierter ist, habe ich statt %-10s  %-5s verwendet,
			//um die gleiche Formatierung zu erhalten.
	}

}

	