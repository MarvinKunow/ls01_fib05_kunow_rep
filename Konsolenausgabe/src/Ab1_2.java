
public class Ab1_2 {

		public static void main(String[] args) {
			
			System.out.printf("%-1s = %-20s = %5d\n", "0!", "", 1);
			System.out.printf("%-1s = %-20s = %5d\n", "1!", "1", 1);
			System.out.printf("%-1s = %-20s = %5d\n", "2!", "1 * 2", 1 * 2);
			System.out.printf("%-1s = %-20s = %5d\n", "3!", "1 * 2 * 3", 1 * 2 * 3);
			System.out.printf("%-1s = %-20s = %5d\n", "4!", "1 * 2 * 3 * 4", 1 * 2 * 3 * 4);
			System.out.printf("%-1s = %-20s = %5d\n", "5!", "1 * 2 * 3 * 4 * 5", 1 * 2 * 3 * 4 * 5);

		}

	}

